This repository contains the slides of the course.

### Notes

The course will be in english. At least the slides will be. This is for some specific reasons. 
The most important is that it is easier to discuss cutting edge technologies in english. The
second reason (less important) is that I can copy paste thing without translastion. The third
important reason is that good ressources you will find will be in english. So you'd better start
mastering english.

### Documents

- The slides of the courses are [here](https://gitlab.com/m4207/cours/raw/master/files/cours_m4207.pdf)


### gitlab
[workflow](https://www.gitlab.com/help/workflow/README.md)